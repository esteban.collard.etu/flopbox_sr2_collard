#!/bin/sh

echo "enregistrer le serveur ftp localhost"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?\"port=2121&id=anonymous&pass=anonymous\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?"port=2121&id=anonymous&pass=anonymous"

echo "creer un dossier"
echo "curl http://localhost:8080/FlopBox/serveurFtp/rename/localhost/dossier1?dir=/dossier2"
curl http://localhost:8080/FlopBox/serveurFtp/rename/localhost/dossier1?dir=/dossier2

echo "renomage un dossier"
echo "curl http://localhost:8080/FlopBox/serveurFtp/rename/localhost/dossier2?dir=/dossier1"
curl http://localhost:8080/FlopBox/serveurFtp/rename/localhost/dossier2?dir=/dossier1

echo "supprimer le serveur ftp de la plateforme"
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost
