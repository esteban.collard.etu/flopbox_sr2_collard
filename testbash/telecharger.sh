#!/bin/sh

echo "enregistrer le serveur ftp ftp.ubuntu.com"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/ftp.ubuntu.com?\"id=anonymous&pass=anonymous@domain.com\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/ftp.ubuntu.com?"id=anonymous&pass=anonymous@domain.com"

echo "telechargent du fichier /ubuntu-ports/dists/bionic/Release.gpg sauvegarde dans /tmp/Release.gpg"
echo "curl http://localhost:8080/FlopBox/serveurFtp/download/ftp.ubuntu.com/ubuntu-ports/dists/bionic/Release.gpg?file=Release.gpg"
curl http://localhost:8080/FlopBox/serveurFtp/download/ftp.ubuntu.com/ubuntu-ports/dists/bionic/Release.gpg?file=Release.gpg

echo "supprimer le serveur ftp de la plateforme"
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/ftp.ubuntu.com"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/ftp.ubuntu.com
