#!/bin/sh

echo "création de l'alias alias1 pour le serveur ftp.ubuntu.com"
echo "curl -X POST http://localhost:8080/FlopBox/alias?\"alias=alias1&serveur=ftp.ubuntu.com\""
curl -X POST http://localhost:8080/FlopBox/alias?"alias=alias1&serveur=ftp.ubuntu.com"

echo "affichage de tout les alias\n"
echo "curl http://localhost:8080/FlopBox/alias"
curl http://localhost:8080/FlopBox/alias

echo "suppression alias alias1"
echo "curl -X DELETE http://localhost:8080/FlopBox/alias?\"alias=alias1\""
curl -X DELETE http://localhost:8080/FlopBox/alias?"alias=alias1"

echo "affichage de tout les alias après suppression"
echo "curl http://localhost:8080/FlopBox/alias"
curl http://localhost:8080/FlopBox/alias