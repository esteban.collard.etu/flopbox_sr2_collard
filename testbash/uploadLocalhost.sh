#!/bin/sh

echo "enregistrer le serveur ftp localhost"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?\"port=2121&id=anonymous&pass=anonymous\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?"port=2121&id=anonymous&pass=anonymous"

echo "upload d'un fichier texte"
echo "curl http://localhost:8080/FlopBox/serveurFtp/upload/localhost/texteTEST.txt?dir=./testbash/texteTEST.txt"
curl http://localhost:8080/FlopBox/serveurFtp/upload/localhost/texteTEST.txt?dir=./testbash/texteTEST.txt

echo "affichage du fichier texte\n\n"
echo "cat /tmp/texte1.txt"
cat /tmp/texte1.txt

echo "\n\nupload d'un fichier .jpg"
echo "curl http://localhost:8080/FlopBox/serveurFtp/upload/localhost/imageTEST.jpg?dir=./testbash/imageTEST.jpg"
curl http://localhost:8080/FlopBox/serveurFtp/upload/localhost/imageTEST.jpg?dir=./testbash/imageTEST.jpg

echo "supprimer le serveur ftp de la plateforme"
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost
