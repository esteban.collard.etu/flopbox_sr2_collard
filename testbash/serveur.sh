#!/bin/sh

echo "enregistrer un serveur le serveur ftp ftp.free.fr sans alias"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/ftp.free.fr?\"id=anonymous&pass=anonymous@domain.com\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/ftp.free.fr?"id=anonymous&pass=anonymous@domain.com"

echo "enregistrer un serveur le serveur ftp ftp.ubuntu.com via un alias"
echo "curl -X POST http://localhost:8080/FlopBox/alias?\"alias=alias1&serveur=ftp.ubuntu.com\""
curl -X POST http://localhost:8080/FlopBox/alias?"alias=alias1&serveur=ftp.ubuntu.com"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/toto?\"id=anonymous&pass=anonymous@domain.com\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/toto?"id=anonymous&pass=anonymous@domain.com"

echo "voir les différentes serveur ftp enregistrer"
echo "curl http://localhost:8080/FlopBox/serveurFtp"
curl http://localhost:8080/FlopBox/serveurFtp

echo "supprimer un serveur ftp de la plateforme"
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/ftp.free.fr"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/ftp.free.fr
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/toto"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/toto

echo "voir les différentes serveur ftp enregistrer après suppression"
echo "curl http://localhost:8080/FlopBox/serveurFtp"
curl http://localhost:8080/FlopBox/serveurFtp