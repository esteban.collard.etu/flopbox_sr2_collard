#!/bin/sh

echo "enregistrer le serveur ftp localhost"
echo "curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?\"port=2121&id=anonymous&pass=anonymous\""
curl -X POST http://localhost:8080/FlopBox/serveurFtp/localhost?"port=2121&id=anonymous&pass=anonymous"

echo "telechargent d'un fichier texte"
echo "curl http://localhost:8080/FlopBox/serveurFtp/download/localhost/texte1.txt?dir=texte1.txt"
curl http://localhost:8080/FlopBox/serveurFtp/download/localhost/texte1.txt?file=texte1.txt

echo "\n\ntelechargent d'un fichier .jpg"
echo "curl http://localhost:8080/FlopBox/serveurFtp/download/localhost/image1.jpg?file=/image1.jpg"
curl http://localhost:8080/FlopBox/serveurFtp/download/localhost/image1.jpg?file=/image1.jpg

echo "supprimer le serveur ftp de la plateforme"
echo "curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost"
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/localhost
