package ressource;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import sr2.Main;

/**
 * gestionnaire l'intéraction avec le client pour les alias
 * @author collard
 */
@Path("/alias")
public class AliasRessource {
	
	/**
	 * creation d'un alias
	 * @param alias String : nom de l'alias
	 * @param serveur String : nom du serveur
	 * @return String : message de retour
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public String creationAlias(@QueryParam("alias") String alias, @QueryParam("serveur") String serveur) {
		if(serveur != null && alias != null) {
			if(Main.setAlias(alias, serveur)) {
				return "200 L'alias "+alias+" à a ete creer.\n";				
			}
			return "400 Le nom d'alias "+alias+" est déja utiliser\n";
		}
		return "400 Parametre alias non valiede\n";
	}
	
	/**
	 * affiche la liste des alias
	 * @return la liste des alias
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getAlias() {
		HashMap<String, String> hashMapAlias = Main.getAlias();
		if(hashMapAlias.size()==0) { return "Il n'y a plus d'allias\n"; }
		String valRet = "Liste des alias :\n";
		for(String alias : hashMapAlias.keySet()) {
			valRet += alias + " - " + hashMapAlias.get(alias) + "\n";
		}
		return valRet+"\n";
	}
	
	/**
	 * supprime un alias
	 * @param alias String : nom de l'alias a supprimer
	 * @return String : message de retour
	 */
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteAlias(@QueryParam("alias") String alias) {
		if(Main.removeAlias(alias)) {
			return "L'alias "+alias+" a bien ete supprimer.\n";
		}
		return "L'alias "+alias+" n'existe pas.\n";
	}
}