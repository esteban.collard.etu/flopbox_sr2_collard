package ressource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import serveurFtp.ServeurFtp;
import sr2.Main;

import java.io.File;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;

/**
 * gestionnaire l'intéraction avec le client pour les serveurs
 * @author collard
 */
@Path("/serveurFtp")
public class ServeurFtpRessource {

	/**
	 * affiche la liste des serveurs
	 * @return la liste des serveurs
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getServeurFtp() {
		HashMap<String, ServeurFtp> hashMapServeur = Main.getServeur();
		if(hashMapServeur.size()==0) { return "Il n'y a plus de serveur\n"; }
		String valRet = "Liste des serveurs :\n";
		for(String serveur : hashMapServeur.keySet()) {
			valRet += serveur + "\n";
		}
		return valRet+"\n";
	}

	/**
	 * supprime un serveur
	 * @param serveur String : nom du serveur
	 * @return String : message de retour
	 */
	@DELETE
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{nomServeur}")
	public String deleteServeur(@PathParam("nomServeur") String serveur) {
		if(Main.removeServeur(serveur)) {
			return "Le serveur "+serveur+" a bien ete supprimer.\n";
		}
		return "Le serveur "+serveur+" n'existe pas.\n";
	}

	/**
	 * creation d'une passerelle
	 * @param serveur String : nom du serveur
	 * @param port int : numero du port de connexion
	 * @param id String : identifiant pour la connexion
	 * @param pass String : mot de passe pour la connexion
	 * @return String : message de retour
	 */
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{nomServeur}")
	public String creationConnectionServeur(
			@PathParam("nomServeur") String serveur, 
			@QueryParam("port") String port,
			@QueryParam("id") String id,
			@QueryParam("pass") String pass ) {

		ServeurFtp s;

		if(Main.aliasContain(serveur)) {
			serveur = Main.getOneAlias(serveur);
		}
		if(Main.serveurContain(serveur)) {
			return "400 connection avec le serveur deja creer\n";
		}

		if(port != null && id != null && pass != null) { s = new ServeurFtp(serveur, Integer.parseInt(port), id, pass); }
		else if( id != null && pass != null) { s = new ServeurFtp(serveur, id, pass); }
		//else if(port != null) { s = new ServeurFtp(serveur, Integer.parseInt(port)); }
		//else{ s = new ServeurFtp(serveur); }
		else { return "400 parametre invalide connection impossible"; }
		Main.setServeur(serveur, s);

		return "200 connection avec le serveur creer\n";
	}

	/**
	 * retourne la list des fichiers et dossier, d'un dossier sur un serveur ftp
	 * @param serveur String : nom du serveur
	 * @param directory String : repertoire a afficher
	 * @return String : message de retour
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/{nomServeur}/{path: .*}")
	public String getListDirectory(@PathParam("nomServeur") String serveur, @PathParam("path") String directory) {
		if(Main.aliasContain(serveur)) {
			serveur = Main.getOneAlias(serveur);
		}

		if(!Main.serveurContain(serveur)) {
			return "400 Pas d'acces au serveur "+serveur+" ce serveur\n";
		}
		else {
			ServeurFtp s = Main.getOneServeur(serveur);
			return s.listDirectoryFile(directory);			
		}
	}

	/**
	 * telecharge un fichier depuis un serveur ftp
	 * @param serveur String : nom du serveur
	 * @param file String : chemin du fichier à telecharger
	 * @param dir String : chemin d'ecriture du fichier
	 * @return String : message de retour
	 */
	@GET
	@Produces({MediaType.APPLICATION_OCTET_STREAM, MediaType.TEXT_PLAIN})
	@Path("/download/{nomServeur}/{path: .*}")
	public Response downloadFile(
			@PathParam("nomServeur") String serveur, 
			@PathParam("path") String file,
			@QueryParam("file") String fileName) {

		
		if(fileName!=null) {
			if(Main.aliasContain(serveur)) {
				serveur = Main.getOneAlias(serveur);
			}

			if(!Main.serveurContain(serveur)) {
				return Response.status(Response.Status.OK)
						.entity("400 Pas d'acces au serveur "+serveur+" ce serveur\n")
						.build();
			}
			else {
				ServeurFtp s = Main.getOneServeur(serveur);
				String m = s.downloadFile(file,fileName);
				File f = new File("/tmp/"+fileName);
				return  Response.status(Response.Status.OK)
						.entity((Object) f)
						.header("Content-Disposition", "attachment; filename=\""+f.getName()+"\"")
						.build();
			}			
		}
		else {
			return Response.status(Response.Status.NOT_FOUND)
					.entity("400 pas de nom de fichier\n")
					.build();
		}
	}

	/**
	 * upload un fichier sur un serveur ftp
	 * @param serveur String : nom du serveur
	 * @param file String : chemin d'ecriture du fichier
	 * @param dir String : chemin de lecture du fichier
	 * @return String : message de retour
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/upload/{nomServeur}/{path: .*}")
	public String uploadFile(
			@PathParam("nomServeur") String serveur, 
			@PathParam("path") String file,
			@QueryParam("dir") String dir) {

		if(dir!=null) {
			if(Main.aliasContain(serveur)) {
				serveur = Main.getOneAlias(serveur);
			}

			if(!Main.serveurContain(serveur)) {
				return "400 Pas d'acces au serveur "+serveur+" ce serveur\n";
			}
			else {
				ServeurFtp s = Main.getOneServeur(serveur);
				return s.uploadFile(file,dir);			
			}			
		}
		else {
			return "400 pas de repertoire de retour.\n";
		}
	}
	
	/*
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/upload/{nomServeur}/{path: .*}")
	public String uploadFile(
			@PathParam("nomServeur") String serveur, 
			@PathParam("path") String file,
			@FormDataParam("file") InputStream dir) {
			//@QueryParam("dir") String dir) {

		if(dir!=null) {
			if(Main.aliasContain(serveur)) {
				serveur = Main.getOneAlias(serveur);
			}

			if(!Main.serveurContain(serveur)) {
				return "400 Pas d'acces au serveur "+serveur+" ce serveur\n";
			}
			else {
				ServeurFtp s = Main.getOneServeur(serveur);
				return s.uploadFile(file,dir);			
			}			
		}
		else {
			return "400 pas de repertoire de retour.\n";
		}
	}
	 */

	/**
	 * creer un dossier sur un serveur ftp
	 * @param serveur String : nom du serveur
	 * @param dir String : chemin du nouveau dossier
	 * @return String : message de retour
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/mkdir/{nomServeur}")
	public String mkdir(
			@PathParam("nomServeur") String serveur, 
			@QueryParam("dir") String dir) {

		if(Main.aliasContain(serveur)) {
			serveur = Main.getOneAlias(serveur);
		}

		if(!Main.serveurContain(serveur)) {
			return "400 Pas d'acces au serveur "+serveur+"\n";
		}
		else {
			ServeurFtp s = Main.getOneServeur(serveur);
			return s.mkdir(dir);
		}			
	}
	
	/**
	 * supprime un dossier sur un serveur ftp
	 * @param serveur String : nom du serveur
	 * @param dir String : chemin du dossier a supprimer
	 * @return String : message de retour
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/remove/{nomServeur}")
	public String remove(
			@PathParam("nomServeur") String serveur, 
			@QueryParam("dir") String dir) {

		if(Main.aliasContain(serveur)) {
			serveur = Main.getOneAlias(serveur);
		}

		if(!Main.serveurContain(serveur)) {
			return "400 Pas d'acces au serveur "+serveur+"\n";
		}
		else {
			ServeurFtp s = Main.getOneServeur(serveur);
			return s.remove(dir);
		}			
	}
		
	/**
	 * renome un fichier ou un dossier
	 * @param serveur String : nom du serveur
	 * @param nameFile String : nom fichier a renomer
	 * @param newName String : nouveau nom
	 * @return String : message de retour
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/rename/{nomServeur}/{path: .*}")
	public String rename(
			@PathParam("nomServeur") String serveur, 
			@PathParam("path") String nameFile,
			@QueryParam("newName") String newName) {

		if(Main.aliasContain(serveur)) {
			serveur = Main.getOneAlias(serveur);
		}

		if(!Main.serveurContain(serveur)) {
			return "400 Pas d'acces au serveur "+serveur+"\n";
		}
		else {
			ServeurFtp s = Main.getOneServeur(serveur);
			return s.rename(nameFile, newName);
		}			
	}
}