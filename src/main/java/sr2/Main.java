package sr2;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import serveurFtp.ServeurFtp;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

/**
 * Main class.
 *
 */
public class Main {
	// Base URI the Grizzly HTTP server will listen on
	public static String BASE_URI = "http://localhost:8080/FlopBox/";

	private static HashMap<String, ServeurFtp> listServeur = new HashMap<String, ServeurFtp>();
	private static HashMap<String, String> aliasServeur = new HashMap<String, String>();
	

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer() {
		// create a resource config that scans for JAX-RS resources and providers
		// in sr2 package
		final ResourceConfig rc = new ResourceConfig().packages("ressource");

		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
	}

	/**
	 * Main method.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		if(args.length<=3) {
			if(args.length==2) {
				BASE_URI = "http://localhost:"+args[0]+args[1];
			}
			else if(args.length==1) {
				BASE_URI = "http://localhost:"+args[0]+"/FlopBox/";
			}

			final HttpServer server = startServer();
			System.out.println(String.format("Jersey app started with WADL available at "
					+ "%sapplication.wadl\nHit enter to stop it...", BASE_URI));

			System.in.read();
			server.stop();
		}

	}

	public static HashMap<String, String> getAlias() {
		return aliasServeur;
	}
	
	public static String getOneAlias(String alias) {
		return aliasServeur.get(alias);
	}
	
	public static boolean setAlias(String alias, String serveur) {
		if(!aliasServeur.containsKey(alias)) {
			aliasServeur.put(alias, serveur);
			return true;			
		}
		return false;
	}
	
	public static boolean aliasContain(String alias) {
		return aliasServeur.containsKey(alias);
	}
	
	public static boolean removeAlias(String alias) {
		if(aliasServeur.containsKey(alias)) {
			aliasServeur.remove(alias);
			return true;
		}
		return false;
	}
	
	public static HashMap<String, ServeurFtp> getServeur() {
		return listServeur;
	}
	
	public static ServeurFtp getOneServeur(String serveur) {
		return listServeur.get(serveur);
	}
	
	public static boolean serveurContain(String serveur) {
		return listServeur.containsKey(serveur);
	}
	
	public static boolean setServeur(String serveur, ServeurFtp serveurFtp) {
		if(!listServeur.containsKey(serveur)) {
			listServeur.put(serveur, serveurFtp);
			return true;			
		}
		return false;
	}
	
	public static boolean removeServeur(String serveur) {
		if(listServeur.containsKey(serveur)) {
			listServeur.remove(serveur);
			return true;
		}
		return false;
	}
}
