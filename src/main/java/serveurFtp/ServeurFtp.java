package serveurFtp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 * Classe permetant de creer une passerelle avec un serveur ftp
 * @author collard
 */
public class ServeurFtp {
	/**
	 * String : nom du serveur
	 */
	private String nameServeur;
	
	/**
	 * int : numero de port
	 */
	private int port;
	
	/**
	 * String : nom d'utilisateur
	 */
	private String nameUtilisateur;
	
	/**
	 * String : mot de passe
	 */
	private String passUtilisateur;
	
	/**
	 * FTPClient : accés au serveur ftp
	 */
	private FTPClient ftp;

	/**
	 * creer une passerelle avec un serveur ftp
	 * @param nameServ String : nom du serveur ftp
	 * @param p int : numero port serveur ftp
	 * @param nameUser String : nom d'utilisateur
	 * @param passUser String : mot de passe
	 */
	public ServeurFtp(String nameServ) {
		this.nameServeur = nameServ;
		this.port = 21;
		this.nameUtilisateur = "anonymous";
		this.passUtilisateur = "anonymous@domain.com";
		this.ftp = new FTPClient();
	}

	/**
	 * creer une passerelle avec un serveur ftp
	 * @param nameServ String : nom du serveur ftp
	 * @param p int : numero port serveur ftp
	 */
	public ServeurFtp(String nameServ, int p) {
		this.nameServeur = nameServ;
		this.port = p;
		this.nameUtilisateur = "anonymous";
		this.passUtilisateur = "anonymous@domain.com";
		this.ftp = new FTPClient();
	}

	/**
	 * creer une passerelle avec un serveur ftp
	 * @param nameServ String : nom du serveur ftp
	 * @param nameUser String : nom d'utilisateur
	 * @param passUser String : mot de passe
	 */
	public ServeurFtp(String nameServ, String nameUser, String passUser) {
		this.nameServeur = nameServ;
		this.port = 21;
		this.nameUtilisateur = nameUser;
		this.passUtilisateur = passUser;
		this.ftp = new FTPClient();
	}

	/**
	 * creer une passerelle avec un serveur ftp
	 * @param nameServ String : nom du serveur ftp
	 * @param p int : numero port serveur ftp
	 * @param nameUser String : nom d'utilisateur
	 * @param passUser String : mot de passe
	 */
	public ServeurFtp(String nameServ, int p, String nameUser, String passUser) {
		this.nameServeur = nameServ;
		this.port = p;
		this.nameUtilisateur = nameUser;
		this.passUtilisateur = passUser;
		this.ftp = new FTPClient();
	}

	/**
	 * fonction qui connecte le client au serveur
	 * @return boolean : true si la connection a reussi false sinon
	 */
	private boolean connectionServeur() {
		try {
			this.ftp.connect(this.nameServeur, this.port);
			boolean isOK = this.ftp.login(this.nameUtilisateur, this.passUtilisateur);
			return isOK;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * fonction qui deconnecte le client au serveur
	 */
	private void deconnectionServeur() {
		if(this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			}
			catch(IOException e) {
			}
		}
	}

	/**
	 * retourne la list des fichiers et dossier, d'un dossier sur un serveur ftp
	 * @param parent String : repertoire a afficher
	 * @return String : code + contenu des repertoire 
	 */
	public String listDirectoryFile(String parent) {
		if(this.connectionServeur()) {
			String valRet = "200\n\n";
			try {
				FTPFile[] directory = this.ftp.listDirectories(parent);
				for (FTPFile d : directory){
					valRet += d+"\n";
				}
				FTPFile[] file = this.ftp.listFiles(parent);
				for (FTPFile f : file){
					valRet += f+"\n";
				}
			} catch (IOException e) {
				return "401 Erreur lors de la connexion au serveur.\n";
			}
			this.deconnectionServeur();
			return valRet;
		}
		return "400 connection au serveur impossible.\n";
	}

	/**
	 * telecharge un fichier depuis un serveur ftp
	 * @param chemin String : chemin du fichier à telecharger
	 * @param cheminEcriture String : chemin d'ecriture du fichier
	 * @return String : message de retour
	 */
	public String downloadFile(String chemin, String fileName) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				OutputStream file = new FileOutputStream("/tmp/"+fileName);
				this.ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				this.ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
				if(this.ftp.retrieveFile(chemin, file)) {
					this.deconnectionServeur();
					file.close();
					return "200 Telechargement terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				file.close();
				return code+" Telechargment impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 Telechargment impossible.\n";
			}
		}
		return "400 Telechargment impossible.\n";
	}
	
	/**
	 * envoi un fichier au serveur ftp
	 * @param cheminEcriture String : chemin du fichier a ecrire
	 * @param chemin String : chemin de lecture du fichier
	 * @return String : message de retour
	 */
	public String uploadFile(String cheminEcriture, String chemin) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				InputStream file = new FileInputStream(new File(chemin));
				this.ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				this.ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
				this.ftp.enterLocalPassiveMode();
				if(this.ftp.storeFile(cheminEcriture, file)) {
					this.deconnectionServeur();
					file.close();
					return "200 Upload terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				file.close();
				return code+" Upload impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 Upload impossible nom fichier invalide.\n";
			}
		}
		return "400 Upload impossible connexion impossible.\n";
	}
	
	/*
	public String uploadFile(String cheminEcriture, InputStream file) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				//InputStream file = new FileInputStream(new File(chemin));
				this.ftp.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);
				this.ftp.setFileTransferMode(FTP.BINARY_FILE_TYPE);
				this.ftp.enterLocalPassiveMode();
				if(this.ftp.storeFile(cheminEcriture, file)) {
					this.deconnectionServeur();
					file.close();
					return "200 Upload terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				file.close();
				return code+" Upload impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 Upload impossible nom fichier invalide.\n";
			}
		}
		return "400 Upload impossible connexion impossible.\n";
	}*/
	
	/**
	 * creer un repertoire
	 * @param chemin String : chemin du dossier a creer
	 * @return String : message de retour
	*/ 
	
	public String mkdir(String chemin) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				if(this.ftp.makeDirectory(chemin)) {
					this.deconnectionServeur();
					return "200 creation terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				return code+" creation impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 creation impossible nom dossier invalide.\n";
			}
		}
		return "400 creation impossible.\n";
	}
	
	/**
	 * supprime un repertoire
	 * @param chemin String : chemin du dossier a supprimer
	 * @return String : message de retour
	*/ 
	public String remove(String chemin) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				if(this.ftp.removeDirectory(chemin)) {
					this.deconnectionServeur();
					return "200 creation terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				return code+" creation impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 creation impossible nom dossier invalide.\n";
			}
		}
		return "400 creation impossible.\n";
	}
	
	/**
	 * renome un fichier ou un dossier
	 * @param nameFile String : nom du fichier
	 * @param newName String : nouveau nom
	 * @return String : message de retour
	 */
	public String rename(String nameFile, String newName) {
		if(this.connectionServeur()) {
			boolean connexionfailed = !FTPReply.isPositiveCompletion(ftp.getReplyCode());
			if(connexionfailed) {
				return "400 impossible de se connecter au serveur.\n";
			}
			try {
				if(this.ftp.rename(nameFile, newName)) {
					this.deconnectionServeur();
					return "200 rename terminé.\n";
				}
				int code = this.ftp.getReplyCode();
				this.deconnectionServeur();
				return code+" rename impossible.\n";
			} catch (IOException e) {
				e.printStackTrace();
				return "400 rename impossible nom invalide.\n";
			}
		}
		return "400 rename impossible.\n";
	}
}