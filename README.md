# FlopBox_SR2_Collard

## Introduction

L'objectif du projet est de réaliser une plateforme FlopBox en utilisant un style architectural REST afin de centraliser la gestion de serveurs FTP.


## Compilation

`mvn package` permet de généré la documentation des classes.

## Exécution

`java -jar target/simple-service-jar-with-dependencies.jar` suivi de paramètre optionnel, permet d'exécuter le programme.
Les paramètres optionnels sont les suivants :
* numero du port (optionnel)
* chemin de l'URL (optionnel)

Afin d'exécuter les différentes fonctionnalités de FlopBox vous pouvez utiliser des commandes via curl.
Vous avez des exemples dans les fichiers bash dans le dossier dossierTest

Si vous le souhaiter vous avez une démo de l'exécution du programme dans le dossier docs

Exemple commande :

curl -X POST http://localhost:8080/FlopBox/alias?"alias=alias1&serveur=ftp.ubuntu.com"
curl http://localhost:8080/FlopBox/alias
curl -X DELETE http://localhost:8080/FlopBox/alias?"alias=alias1"
curl http://localhost:8080/FlopBox/alias
curl -X POST http://localhost:8080/FlopBox/alias?"alias=alias1&serveur=ftp.ubuntu.com"
curl -X POST http://localhost:8080/FlopBox/serveurFtp/toto?"id=anonymous&pass=anonymous@domain.com"
curl http://localhost:8080/FlopBox/serveurFtp
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/ftp.free.fr
curl -X DELETE http://localhost:8080/FlopBox/serveurFtp/toto

## Architecture du projet

La projet ce distingue en plusieur classe :
* la classe AliasRessource qui gère les alias
* la classe ServeurFtpRessource qui gère les connexions avec les serveurs ftp
* la classe ServeurFtp permet de manipuler un serveur ftp

### Gestion d'erreurs
Les erreurs sont gérer via un system de try catch. Dans le cas où une erreurs est détecté les fonctions vont renvoyer au client un code d'erreurs.
En voici un exemple :

### Code sample

Cette fonction permet de récuperer la liste des fichiers d'un dossier

		@GET
		@Produces(MediaType.TEXT_PLAIN)
		public String getServeurFtp() {
			HashMap<String, ServeurFtp> hashMapServeur = Main.getServeur();
			if(hashMapServeur.size()==0) { return "Il n'y a plus de serveur\n"; }
			String valRet = "Liste des serveurs :\n";
			for(String serveur : hashMapServeur.keySet()) {
				valRet += serveur + "\n";
			}
			return valRet+"\n";
		}


Deconnexion propre au serveur FTP

		int code = this.ftp.getReplyCode();
		this.deconnectionServeur();
		file.close();
		return code+" Telechargment impossible.\n";

